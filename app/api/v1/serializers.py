from pydantic import BaseModel


class ResolutionSerializers(BaseModel):
    """ResolutionSerializers schema"""
    quality: int = None


class UrlSerializers(BaseModel):
    """UrlSerializers schema"""
    url: str = None
