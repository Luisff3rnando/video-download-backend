import json
import falcon

# Serializer
from .serializers import ResolutionSerializers, UrlSerializers
from app.core.handler import Download


class VideoDownload:
    def on_get(self, req, resp, **kwargs):
        try:
            video_yt = Download.get_video(kwargs, req.media)
            resp.body = json.dumps(video_yt)
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.body = json.dumps(dict(
                error=True,
                description=str(e)
            ))
            resp.status = falcon.HTTP_422

    def on_post(self, req, resp):
        try:
            serializer = UrlSerializers(**req.media)
            resolution = Download.get_res(serializer.dict())
            resp.body = json.dumps(resolution)
            resp.status = falcon.HTTP_201
         except Exception as e:
            resp.body = json.dumps(dict(
                error=True,
                description=str(e)
            ))
            resp.status = falcon.HTTP_422


class HealthCheckView:
    """
    Health check resource
    """

    def on_get(self, request, response):
        output = {
            "status": "ok"
        }
        response.body = json.dumps(output)
        response.status = falcon.HTTP_200
