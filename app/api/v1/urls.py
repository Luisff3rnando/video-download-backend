from config import app

from .views import VideoDownload, HealthCheckView

app.add_route(
    '/api/v1/url',
    VideoDownload()
),
app.add_route(
    '/api/v1/url/{resolution}/',
    VideoDownload()
),

app.add_route(
    '/health',
    HealthCheckView()
)
