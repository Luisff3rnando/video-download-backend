from .process import Video


class Download:
    """
    docstring
    """
    @staticmethod
    def get_res(data):
        res = Video().get_resolution(data)
        return res

    @staticmethod
    def get_video(res, url):
        video = Video().get_download(res, url)
        return video
