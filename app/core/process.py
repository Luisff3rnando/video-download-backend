from pytube import YouTube


class Video:

    def get_resolution(self, data):
        url = data['url']
        yt = YouTube(url)
        res = yt.streams.filter(progressive=True)
        list_res = []
        for r in res:
            list_res.append(r.resolution)
        return dict(video=url, resolution=list_res)

    def get_download(self, resolution, url):
        res = resolution['resolution']
        url = url['url']
        yt = YouTube(url)
        try:
            yt.streams[int(res)].download()
            return
        except Exception:
            return "video not found"
