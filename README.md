# Microservice Youtube Download

-   Language: Python3
-   Framework: Falcon

#
# Instalation

1.  Install Virtualenv and activate it

```
python3 -m venv venv
```

```
source venv/bin/activate
```

2.  Install requirements.txt

```
pip3 install - r requirements.txt
```
-   Libraries:
    - falcon
    - gunicorn
    - pydantic
    - pytube


# EndPoint

```
GET /health
```
Response
```
status code : 200
"status:"ok"
```
#
``` 
POST /api/v1/url 
```
Body
```
{
    "url":"youtube.com/abc"
}
```

Response 

```
{
    "url":"youtube.com/abc"
    "resolution":[360,720,1080]
}
```
#
``` 
GET /api/v1/url/{resolution}/
```
Params
```
 resolution = int
```
Body
``` 
{
    "url":"youtube.com/abc"
}
```
#

**run Server**
```
$ gunicorn config --bind=0.0.0.0:8001  
```

