
import falcon
from falcon_cors import CORS

cors = CORS(allow_all_origins=True,
    allow_all_headers=True,
    allow_all_methods=True,)

app = application = falcon.API(middleware=[cors.middleware])


# routing
from app.api.v1 import urls
